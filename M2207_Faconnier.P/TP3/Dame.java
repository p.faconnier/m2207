package tp3;

public class Dame extends Humain {
	// Attribut
	private boolean libre;
	private String situtation;
	
	
	// Constructeur
	public Dame(String nom){
		super(nom);
		boisson = "Martini";
		libre = true;
	}
	
	
	
	// Accesseur
	
	
	
	// Methodes
	void priseEnOtage(){
		libre = false;
		System.out.println("Au secours !");
	}
	
	void estLiberee(){
		libre = true;
		System.out.println("Merci Cow-Boy");
	}
	
	public String quelEstTonNom() {
		return "Bonjour, je suis Miss " + nom; 
	}
	
	void sePresenter(){
		super.sePresenter();
		if (libre == true){
			parler("Actuellement, je suis libre"); 
		}
		else {
			parler("Actuellement, je suis kidnappée");
		}
	}

}
