package tp3;

public class Humain {
	
	// Attribut
	protected String nom, boisson;
	
	
	// Constructeur
	public Humain(String nom){
		this.nom = nom;
		boisson = "lait";
	}
	
	
	// Accesseur
	
	
	
	// Methodes
	public String quelEstTonNom() {
		return "Bonjour, je suis " + nom; 
	}
	
	public String quelleEstTaBoisson(){
		return "ma boisson favorite est le " + boisson;
	}
	
	void parler(String texte){
		System.out.println((nom) + " - " + texte);
	}
	
	void sePresenter(){
		parler(quelEstTonNom() + " et " + quelleEstTaBoisson());
	}
	
	void boire(){
		parler("Ah ! un bon verre de " + boisson + " ! GLOUPS !");	
	}
	
}
