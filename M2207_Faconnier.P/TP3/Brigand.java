package tp3;

public class Brigand extends Humain{
		// Attribut
		private String look;
		private int nbreDameKidnappe;
		private boolean capturer;
		private int recompense;
		
		// Constructeur
		public Brigand(String nom){
			super(nom);
			look = "méchant";
			capturer = false;
			nbreDameKidnappe = 0;
			boisson = "cognac";
			recompense = 100;
		}
		
		
		// Accesseur
		
		
		
		// Methodes
		
		public int getRecompense(){
			return recompense;
		}
		
		public String quelEstTonNom() {
			return "Bonjour, je suis " + nom + " le " + look; 
		}
		
		void sePresenter(){
			super.sePresenter();
			parler("J'ai l'air " + look + " et j'ai enlevé " + nbreDameKidnappe + "dames.");
			parler("Ma tête est mise à prix " + recompense + " $ !!");
		}
		
		void enleve(Dame dame){
			nbreDameKidnappe++;
			recompense = recompense + 100;
			dame.priseEnOtage();
			parler("Ah ah ! " + dame.nom + ", tu es ma prisonnière !");
			
		}
}
