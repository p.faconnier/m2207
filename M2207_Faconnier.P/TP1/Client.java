package tp1;

public class Client {
	String nom,prenom;
	private Compte compteCourant;
	
	// Constructeur
	public Client(String n,String p, Compte c){
		nom = n;
		prenom = p;
		compteCourant = c;
	}
	
	
	// Accesseurs
	public String getNom(){
		return nom;
	}
	
	public String getPrenom(){
		return prenom;
	}
}
