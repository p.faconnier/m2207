package tp1;

public class Compte {

	private int numero;
	private double solde;
	private double decouvert;


	//Constructeur
	public Compte(int Numero){
		solde = 0;
		decouvert = 100;
	}


	// Accesseurs
	public void setDecouvert(double montant){
		decouvert = montant;			//on peut modifier le decouvert grâça à cet accesseur
	}

	public double getDecouvert(){
		return decouvert;
	}

	public int getNumero(){
		return numero;
	}

	public double getSolde(){
		return solde;
	}


	// Methode
	public void afficherSolde(){
		System.out.println(solde);
	}

	public void depot(double montant){

		solde = solde + montant;
	}
	
	public String retrait(double montant){
		if (montant > (solde + decouvert) ){
			return ("Retrait refuser");
		}
		else {
			solde = solde - montant;
			return ("Retrait effectuer");
		}
	}
	
	
}
