package tp1;

public class MaBanque {


	public static void main(String[] args) {
		
		
		Compte compte = new Compte(1);
		compte.depot(40);
		
		
		
		System.out.println("Votre compte posséde : " + compte.getSolde());
		System.out.println("Votre découvert est de :" + compte.getDecouvert());
		
		System.out.println(compte.retrait(200));
		System.out.println("Votre compte posséde : " + compte.getSolde());
		
		
		Compte compte2 = new Compte(2);  //Création du second compte
		compte2.depot(1000);			//On depose 1000 €
		compte2.afficherSolde();		//on affiche le solde
		System.out.println(compte2.retrait(600));	//on fait un retrait de 600€
		compte2.afficherSolde();		//on affiche le solde
		System.out.println(compte2.retrait(700));	//on essaye de faire un retrait qui est refuser 
		compte2.afficherSolde();		// on vérifie si notre solde n'a pas changer
		compte2.setDecouvert(500);		// on augmente notre decouvert de 500€
		System.out.println(compte2.retrait(700));	// on refait un retrait de 700€
		compte2.afficherSolde();		//on affiche le solde restant
		
		


	}

}
