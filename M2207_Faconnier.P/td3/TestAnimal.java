package td3;

public class TestAnimal {

	public static void main(String[] args) {
		
		//animal
		Animal animal = new Animal();
		System.out.println(animal.toString());
		
		System.out.println (animal.affiche());
		System.out.println(animal.cri());
		
		//Chien
		Chien moos = new Chien();
		System.out.println (moos.affiche());
		System.out.println(moos.cri());
		
		
		//Chat 
		Chat neko = new Chat();
		System.out.println (neko.affiche());
		System.out.println (neko.miauler());
		
		//origine
		System.out.println(animal.origine());
		
	}

}
