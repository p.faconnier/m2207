package tp6;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tp5.PlusOuMoinsCher;

public class PanneauServeur extends JFrame implements ActionListener{
	//attribut
	JButton bouton;
	JTextArea texte;
	
	//constructeur
	public PanneauServeur () {
		super();
		this.setTitle("Serveur � Panneau d�affichage");		//Titre
		this.setSize(350, 150);						//Taille defini
		this.setLocation(300, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr��e");
		
		
		Container panneau = getContentPane();
		// panneau.setLayout(new FlowLayout());
		bouton = new JButton("EXIT");
		panneau.add(bouton, BorderLayout.SOUTH);
		texte = new JTextArea();
		texte.append("Le panneau est actif");	
		
		
		//Toujours � mettre � la fin du code 
		this.setVisible(true); 
	}

	
	//methode
	
	//methode main
	public static void main(String[] args) {
		PanneauServeur app = new PanneauServeur();
	}
	//accesseur

}
