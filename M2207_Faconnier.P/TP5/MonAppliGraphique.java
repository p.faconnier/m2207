package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;


public class MonAppliGraphique extends JFrame {
	//attributs
	JButton b0;
	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	
	JLabel l0;
	
	JTextField t0;
	
	
	//Constructeurs
	public MonAppliGraphique () {
		super();
		this.setTitle("Ma premiere fenetre");		//Titre
		this.setSize(400, 200);						//Taille de base
		this.setLocation(20, 20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr��e");
		
		// Bouton
		b0 = new JButton("Bouton 0");			//Cr�e le bouton
		b1 = new JButton("Bouton 1");
		b2 = new JButton("Bouton 2");
		b3 = new JButton("Bouton 3");
		b4 = new JButton("Bouton 4");
		
		Container panneau = getContentPane();
		// panneau.setLayout(new FlowLayout());				
		panneau.setLayout(new GridLayout(3,2)) ;
		
		panneau.add(b0);						//associe le bouton b0 au panneau
		panneau.add(b1);
		panneau.add(b2);
		panneau.add(b3);
		panneau.add(b4);
		
		
		//JLabel
		l0 = new JLabel("Je suis un JLabel");
		t0 = new JTextField("Tapez le texte ici !");
		
		// panneau.add(l0, BorderLayout.CENTER);
		// panneau.add(t0, BorderLayout.CENTER);
		
		//Toujours � mettre � la fin du code 
		this.setVisible(true); 
	}
	
	//Methodes
	
	
	//methode main
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique();
	}
	
	
	//accesseur
	
	

}
