package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;


public class PlusOuMoinsCher extends JFrame implements ActionListener{
	//attributs
	JButton b0;
	JLabel l0;	
	JTextField l1;
	JLabel l2;
	int nbreSaisie;
	int inconnu;
	
	//constructeur
	public PlusOuMoinsCher () {
		super();
		this.setTitle("Plus cher ou moins cher");		//Titre
		this.setSize(350, 150);						//Taille defini
		this.setLocation(300, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr��e");
		
		
		Container panneau = getContentPane();
		panneau.setLayout(new GridLayout(2,2));
		
		l0 = new JLabel("Votre proposition :");
		panneau.add(l0);
		l1 = new  JTextField("");
		panneau.add(l1);
		b0 = new JButton("V�rifier !");
		panneau.add(b0);
		l2 = new JLabel("La r�ponse.");
		panneau.add(l2);
		b0.addActionListener(this);
		inconnu = 0 + (int)(Math.random()*((100 - 0)));
		
		System.out.println(inconnu);
		
		//Toujours � mettre � la fin du code 
		this.setVisible(true); 
	}
	
	
	//methode
	public void actionPerformed(ActionEvent e) {
		System.out.println(l1.getText());;
		nbreSaisie = Integer.parseInt(l1.getText());
		
		if ( nbreSaisie > inconnu) {
			l2.setText("Moins cher.");
		}
		else if ( nbreSaisie < inconnu) {
			l2.setText("Plus cher");
		}
		else {
			l2.setText("Vous avez devin� !");
		}
	}
	
	//methode main
	public static void main(String[] args) {
		PlusOuMoinsCher app = new PlusOuMoinsCher();
	}
	//accesseur

}
	