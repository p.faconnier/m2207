package tp5;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener{
	//attributs 
	JButton click0;
	JLabel l1;
	int compteur;
	
	//constructeurs
	public CompteurDeClic () {
		super();
		this.setTitle("Cliquons !");		//Titre
		this.setSize(200, 100);						//Taille defini
		this.setLocation(20, 20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fen�tre est cr��e");
		
		
		Container panneau = getContentPane();
		panneau.setLayout(new FlowLayout());
		
		
		click0 = new JButton("Click !");
		panneau.add(click0);
		click0.addActionListener(this);
		
		l1 = new JLabel("Vous avez cliqu� 0 fois");
		panneau.add(l1);
		
		
		//Toujours � mettre � la fin du code 
		this.setVisible(true); 
	}
	
	
	//Methodes
	public void actionPerformed(ActionEvent e) {
		compteur ++;
		System.out.println("Une action a �t� d�tect�e");
		l1.setText("Vous avez cliqu� " + compteur + " fois.");
	}
	
	//methodes main
	public static void main(String[] args) {
		CompteurDeClic app = new CompteurDeClic();
	}
	//accesseur	

}
