package tp2;

public class Cercle extends Forme {
	
	
	//attribut
	private double rayon;
	
	//Constructeur
	public Cercle(){
		super();
		rayon = 1.0;
	}
	
	public Cercle(double r){
		rayon = r;
	}
	
	public Cercle(double r, String couleur, boolean coloriage){
		super(couleur, coloriage);
		rayon = r;
	}
	//Accesseurs
	
	public double getRayon(){
		return rayon;
	}
	
	public void setRayon(double k){
		rayon = k;
	}
	
	
	//Methodes
	public String seDecrire(){
    	return "Un Cercle de rayon " + rayon + " est issue d'une forme de couleur " + getCouleur() + " et de coloriage " + isColoriage();
    }
	
	public double calculerAire(){
		return (rayon*rayon*Math.PI);
	}
	
	public double calculerPerimetre(){
		return (rayon*2*Math.PI);
	}
}
