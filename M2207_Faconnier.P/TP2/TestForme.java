package tp2;

public class TestForme {

	public static void main(String[] args) {
        
		Forme f1 = new Forme("orange", true);
        Forme f2 = new Forme("vert", false);
        
        System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
        System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());
        
        //Exercice 1.5
        
        f1.setCouleur("rouge");
        f1.setColoriage(false);
        System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
        
        //Exercice 1.6
        System.out.println(f1.seDecrire());
        System.out.println(f2.seDecrire());
        
        //Exercice 2.5
        Cercle c1 = new Cercle();
        System.out.println("Un Cercle de rayon " + c1.getRayon() + " est issue d'une Forme de couleur " + c1.getCouleur() + " et de coloriage " + c1.isColoriage());
        
        //Exercice 2.6
        System.out.println(c1.seDecrire());
        
        //Exercice 2.7
        Cercle c2 = new Cercle(2.5);
        System.out.println(c2.seDecrire());
        
        //Exercice 2.9
        Cercle c3 = new Cercle(3.2, "jaune", false);
        System.out.println(c3.seDecrire());
        
        //Exercice 2.11
        System.out.println("L'aire de c2 est de : " + c2.calculerAire());
        System.out.println("Le perimetre de c2 est de : " + c2.calculerPerimetre());
        System.out.println("L'aire de c3 est de : " + c3.calculerAire());
        System.out.println("Le perimetre de c3 est de : " + c3.calculerPerimetre());
	}

}
