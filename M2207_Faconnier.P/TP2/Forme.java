package tp2;

public class Forme {
    private String couleur ;
    private boolean coloriage;
    
    //Constructeur
    public Forme() {
        couleur="orange";
        coloriage=true;
        }
    
    public Forme(String c,boolean r){
        couleur = c;
        coloriage = r;
        }
        
    //Accesseurs
    
    public String getCouleur(){
        return couleur;
    }
    
    public void setCouleur(String c){
        couleur = c;
    }
    
    public boolean isColoriage(){
        return coloriage;
    }
    
    public void setColoriage(boolean b){
        coloriage = b;
    }
    
    //Methodes
    public String seDecrire(){
    	return "Une forme de couleur " + couleur + " et de coloriage " + coloriage;
    }
}
